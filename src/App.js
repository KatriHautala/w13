import './App.css';

function App() {
  //here is an exmple how to create an array
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  return (
    <div className="App">
      <h1>Table of weekdays</h1>
      <hr/>
        <table>
          <thead>
            <tr>
              <th>
                Weekday
              </th>
            </tr>
          </thead>
          <tbody>
            {weekdays.map((_day, index) => (
              <tr>
                <td>
                  {_day}
                </td>
              </tr>
            
            ))}
          </tbody>
        </table>
    </div>
  );
}

export default App;
